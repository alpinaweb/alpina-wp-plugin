<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://fb.com/luciano.tonet
 * @since      1.0.0
 *
 * @package    Alpina_Wp_Plugin
 * @subpackage Alpina_Wp_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Alpina_Wp_Plugin
 * @subpackage Alpina_Wp_Plugin/public
 * @author     Luciano <luciano@grupoalpina.com.br>
 */
class Alpina_Wp_Plugin_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * An instance of this class should be passed to the run() function
		 * defined in Alpina_Wp_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Alpina_Wp_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		 // Google Fonts
		 wp_enqueue_style('google-fonts-montserrat', "https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i", array(), '4.4.1', 'all' );

		 $options = get_option( '_alpina_settings' );
		 $deps = array();

		 if(@$options["inject_bootstrap"] == 'on') {
			 wp_enqueue_style( 'bootstrap', plugin_dir_url( __FILE__ ).'/css/bootstrap.min.css', array(), microtime(), 'all' );
			 $deps = array('bootstrap');
		 }

		wp_enqueue_style( $this->plugin_name . '-responsive', 	plugin_dir_url( __FILE__ ).'css/responsive.css', array($this->plugin_name), microtime(), 'all' );
		wp_enqueue_style( $this->plugin_name, 	plugin_dir_url( __FILE__ ).'css/alpina-wp-plugin-public.css', $deps, microtime(), 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * An instance of this class should be passed to the run() function
		 * defined in Alpina_Wp_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Alpina_Wp_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		// wp_enqueue_script( 'jquery',						plugin_dir_url( __FILE__ ).'/js/jquery.slim.min.js', array(), $this->version, false );
		// wp_enqueue_script( 'popper', 						plugin_dir_url( __FILE__ ).'/js/popper.min.js', array(), $this->version, false );

		$options = get_option( '_alpina_settings' );
		$deps = array();

		if(@$options["inject_bootstrap"] == 'on') {
			wp_enqueue_script( 'bootstrap-js', 			plugin_dir_url( __FILE__ ).'/js/bootstrap.min.js', array('jquery'), microtime(), false );
			$deps = array( 'bootstrap-js' );
		}

		wp_enqueue_script( $this->plugin_name, 	plugin_dir_url( __FILE__ ).'/js/alpina-wp-plugin-public.js', array( 'bootstrap-js' ), $this->version, false );

	}

	/* Describe what the code snippet does so you can remember later on */
	public function inject_custom_php_styles() {
		require_once plugin_dir_path( dirname( __FILE__ ) ).'public/templates/inc/styles.php';
	}

	public function get_alpina_blog_template ($template) {

		$options = get_option( '_alpina_settings' );

		if ( is_single() && @$options["option_template_is_single"] && is_singular( 'post' ) ) {
			$template = dirname( __FILE__ ) . '/templates/single.php';
		} else if ( is_search() && @$options["option_template_is_search"] && !( isset($_REQUEST['geral']) && !empty($_REQUEST['geral']) )  ) {
			$template = dirname( __FILE__ ) . '/templates/search.php';
		} else if ( is_home() && @$options["option_template_is_home"] ) {
			$template = dirname( __FILE__ ) . '/templates/blog.php';
		} else if ( is_author() && @$options["option_template_is_author"] ) {
			$template = dirname( __FILE__ ) . '/templates/author.php'; // OK
		} else if ( is_tag()
				&& get_post_type( get_the_ID() ) == 'post'
				&& @$options["option_template_is_tag"] ) {
					$template = dirname( __FILE__ ) . '/templates/tag.php';
		} else
		if ( is_category()
				&& get_post_type( get_the_ID() ) == 'post'
				&& @$options["option_template_is_category"] ) {
					$template = dirname( __FILE__ ) . '/templates/category.php';
		} else
		if ( is_archive()
				&& get_post_type( get_the_ID() ) == 'post'
				&& @$options["option_template_is_archive"] ) {
					$template = dirname( __FILE__ ) . '/templates/archive.php';
		}

		return $template;

	}

	function get_alpina_author_template() {
			 global $post;

 			 $page_template = dirname( __FILE__ ) . '/templates/author.php';

			 return $page_template;
	}

	public function alpina_search_form( $html ) {

	  $html = '<form role="search" method="get" class="search-form" action="/">
				      <div class="input-group p-2">
				        <input type="text" class="form-control" placeholder="Buscar..." required value="" name="s" />
				        <div class="input-group-append">
				          <button class="btn btn-outline-secondary" type="submit" id="button-addon2">
				            <i class="fas fa-search"></i>
				          </button>
				        </div>
				      </div>
				    </form>';

		return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );

	}

	//Paginação
	static function get_pagination ($range = 4) {
	  	// $paged - number of the current page
		global $paged, $wp_query;
		$out = '';
		$max_page = '';

	  	// How much pages do we have?
			if ( !$max_page ) {
				$max_page = $wp_query->max_num_pages;
			}
			// We need the pagination only if there are more than 1 page
			if($max_page > 1){
				$out .= '<section class="navigationPage">';
				if(!$paged){
					$paged = 1;
				}
				// On the first page, don't put the First page link
				if($paged != 1){
					$out .= "<a class='arrowLink leftLevel' href=" . get_pagenum_link(1) . "><i class='fas fa-long-arrow-alt-left'></i></a>";
				} else {
					$out .= " <a class='disabled arrowLink leftLevel' href=" . get_pagenum_link(1) . "><i class='fas fa-long-arrow-alt-left'></i></a>";
				}
				// To the previous page
				$out .= get_previous_posts_link(" &laquo; ");
				// We need the sliding effect only if there are more pages than is the sliding range
				if($max_page > $range){
					// When closer to the beginning
					if($paged < $range){
						for($i = 1; $i <= ($range + 1); $i++){
							$out .= "<a href='" . get_pagenum_link($i) ."'";
							if($i==$paged) $out .= "class='current'";
							$out .= ">$i</a>";
						}
					}
					// When closer to the end
					elseif($paged >= ($max_page - ceil(($range/2)))){
						for($i = $max_page - $range; $i <= $max_page; $i++){
							$out .= "<a href='" . get_pagenum_link($i) ."'";
							if($i==$paged) $out .= "class='current'";
							$out .= ">$i</a>";
						}
					}
					// Somewhere in the middle
					elseif($paged >= $range && $paged < ($max_page - ceil(($range/2)))){
						for($i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++){
							$out .= "<a href='" . get_pagenum_link($i) ."'";
							if($i==$paged) $out .= "class='current'";
							$out .= ">$i</a>";
						}
					}
				}
				// Less pages than the range, no sliding effect needed
				else{
					for($i = 1; $i <= $max_page; $i++){
						$out .= "<a href='" . get_pagenum_link($i) ."'";
						if($i==$paged) $out .= "class='current'";
						$out .= ">$i</a>";
					}
				}
				// Next page
				$out .= get_next_posts_link(" &raquo; ");
				// On the last page, don't put the Last page link
				if($paged != $max_page){
					$out .= " <a class='arrowLink rightLevel' href=" . get_pagenum_link($max_page) . "><i class='fas fa-long-arrow-alt-right'></i></a>";
				} else {
					$out .= " <a class='disabled arrowLink rightLevel' href=" . get_pagenum_link($max_page) . "><i class='fas fa-long-arrow-alt-right'></i></a>";
				}
				$out .= '</section>';
		}
		return $out;
	}

	static function has_breadcrumb () {
		return !is_home()
			|| is_tag()
			|| is_day()
			|| is_month()
			|| is_year()
			|| is_author()
			|| (isset($_GET['paged']) && !empty($_GET['paged']))
			|| is_search();
	}

	static function the_breadcrumb () {
		echo '<nav aria-label="breadcrumb">';
		echo '	<ol class="breadcrumb">';
		if (!is_home()) {
			echo '<li class="breadcrumb-item"><a href="';
			echo get_option('home');
			echo '">';
			echo 'Início';
			echo "</a></li>";
			if (is_category() || is_single()) {
				echo '<li class="breadcrumb-item">';
				the_category(' </li><li class="breadcrumb-item"> ');
				if (is_single()) {
					echo '</li><li class="breadcrumb-item">';
					the_title();
					echo '</li>';
				}
			} elseif (is_page()) {
				echo '<li class="breadcrumb-item">';
				echo the_title();
				echo '</li>';
			}
		}
		elseif (is_tag()) { 		single_tag_title(); }
		elseif (is_day()) { 		echo "	<li>Archive for "; the_time('F jS, Y'); echo'</li>'; }
		elseif (is_month()) { 	echo "	<li>Archive for "; the_time('F, Y'); echo'</li>'; }
		elseif (is_year()) { 		echo "	<li>Archive for "; the_time('Y'); echo'</li>'; }
		elseif (is_author()) { 	echo "	<li>Author Archive"; echo'</li>'; }
		elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
														echo "	<li>Blog Archives"; echo'</li>';
		}
		elseif (is_search()) { 	echo "	<li>Search Results"; echo'</li>'; }
		echo '	</ol>';
		echo '</nav>';

	}

}
