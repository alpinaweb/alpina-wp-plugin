<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://fb.com/luciano.tonet
 * @since      1.0.0
 *
 * @package    Alpina_Wp_Plugin
 * @subpackage Alpina_Wp_Plugin/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
