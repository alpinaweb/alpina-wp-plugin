<?php
get_header();
$options = get_option('_alpina_settings');
require(__DIR__.'/inc/subheader.php');
?>

<div id="alpina_wp_plugin_container">
	<div class="container-full py-5">

		<?php if ( Alpina_Wp_Plugin_Public::has_breadcrumb() ) { ?>
			<div class="row">
				<div class="col ">
					<?php echo Alpina_Wp_Plugin_Public::the_breadcrumb(); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row justify-content-between">
			<div class="col col-lg-8">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('card mb-4 py-4 px-3'); ?>>
						<div class="card-body">

							<h6 class="card-subtitle mb-2"><strong><?php the_category( ', ' ); ?></strong></h6>
							<h1 class="card-title h2">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h1>
							<p class="card-text mt-2"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' atrás'; ?> - por: <span class="text-capitalize"><?php the_author_posts_link() ?></span></p>
							<figure class="figure mb-4">
								<a href="#">
									<?php the_post_thumbnail('large', array('class' => 'figure-img img-fluid rounded')); ?>
								</a>
							</figure>

							<div class="entry-content">

								<?php the_content(); ?>

								<?php wp_link_pages(); ?>

								<p class="postmetadata"><?php _e( 'Postado em' ); ?> <?php the_category( ', ' ); ?></p>

							</div>

						</div>
					</article>

				<?php endwhile; else : ?>
					<article class="card mb-4 py-4 px-3">
						<div class="card-body">

							<h6 class="card-subtitle mb-2"><strong><?php the_category( ', ' ); ?></strong></h6>
							<h1 class="card-title h2">
								<strong>
									<?php esc_html_e( 'Desculpe, mas o que você procura não está por aqui...' ); ?>
								</strong>
							</h1>

						</div>
					</article>
				<?php endif; ?>

				<?php require( 'inc/cta.php' ); ?>

				<div class="card mb-4 p-4 author">
					<div class="card-body">
						<div class="row">

							<a class="col-12 col-sm-3 col-md-2 col mb-3" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>">
								<?php if($avatar = get_avatar( get_the_author_meta( 'ID' ) ) !== FALSE): ?>
									<?php echo get_avatar( get_the_author_meta( 'ID' ), '', '', '', array('class' => "mr-0 mr-sm-3 mr-md-5 rounded-circle img-fluid" ) ) ?>
								<?php else: ?>
									<img class="mr-0 mr-sm-3 mr-md-5 rounded-circle img-fluid" src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_thumb.png" alt="...">
								<?php endif; ?>
							</a>

							<div class="col col-md-4 col-xl-3 mb-3">
								<h5 class="mt-0 text-capitalize"><small>Por:</small><br/><?php echo get_the_author() ?></h5>
								<a class="btn btn-primary btn-sm" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>" role="button">
									<i class="fas fa-plus"></i> Posts do autor
								</a>
							</div>

							<div class="col bio"><?php echo get_the_author_meta( 'description' ) ?></div>

						</div>
					</div>
				</div>

				<h4 class="mt-5 py-5">Comentários sobre este post</h4>

				<div id="disqus_thread"></div>
				<script>

					/**
					*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
					*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
					/*
					var disqus_config = function () {
					this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
					this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
				};
				*/
				(function() { // DON'T EDIT BELOW THIS LINE
				var d = document, s = d.createElement('script');
				// s.src = 'https://labs-alpdev-com-br.disqus.com/embed.js';
				<?php
				$options = get_option( '_alpina_settings' );
				$deps = array();

				if(@$options["disqus_js"]) {
					?>
					s.src = '<?php echo $options["disqus_js"] ?>'
					<?php
				} else {
					?>
					s.src = 'https://labs-alpdev-com-br.disqus.com/embed.js';
					<?php
				}
				?>
				s.setAttribute('data-timestamp', +new Date());
				(d.head || d.body).appendChild(s);
			})();
		</script>
		<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


	</div>

	<?php require( 'inc/sidebar.php' ); ?>

</div>
</div>
</div>

<?php get_footer();
