<?php
get_header();
$options = get_option('_alpina_settings');
require(__DIR__.'/inc/subheader.php');
?>

<div id="alpina_wp_plugin_container">
	<div class="container-full py-5">

		<?php if ( Alpina_Wp_Plugin_Public::has_breadcrumb() ) { ?>
			<div class="row">
				<div class="col ">
					<?php echo Alpina_Wp_Plugin_Public::the_breadcrumb(); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row justify-content-between">
			<div class="col col-lg-8">

				<h3 class="text-center my-4"><small>Categoria:</small><br/><?php the_category(', ') ?></h3>
				<?php the_archive_description( '<div class="taxonomy-description text-center">', '</div>' ); ?>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<?php require('inc/card.php'); ?>

				<?php endwhile; else : ?>
					<article class="card mb-4 py-4 px-3">
						<div class="card-body">
							<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
						</div>
					</article>
				<?php endif; ?>

				<?php require( 'inc/cta.php' ); ?>

				<?php echo Alpina_Wp_Plugin_Public::get_pagination(); ?>

			</div>

			<?php require( 'inc/sidebar.php' ); ?>

		</div>
	</div>
</div>

<?php get_footer();
