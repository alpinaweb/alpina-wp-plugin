<?php $options['header_bg'] = $options['header_bg'] ?: plugin_dir_url( __FILE__ ) . '../images/header-bg.jpg'; ?>
<style media="screen">
#alpina_wp_plugin_blog_subheader {
  background: url(<?php echo $options['header_bg']; ?>) center center;
  background-size: contain;
  background-color: #0f1d44;
}
</style>
<header id="alpina_wp_plugin_blog_subheader">
  <div class="container">
    <?php if ( is_active_sidebar( 'alpina_wp_plugin_blog_subheader' ) ) : ?>

      <?php dynamic_sidebar( 'alpina_wp_plugin_blog_subheader' ); ?>

    <?php endif; ?>
  </div>
</header>
