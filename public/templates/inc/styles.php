<?php
$options = get_option('_alpina_settings');
?>
<style media="screen">
  :root {
    --alp-primary: <?php echo $options['main_color'] ?>;
    --alp-secondary-color: <?php echo $options['secondary_color'] ?>;
  }
</style>
