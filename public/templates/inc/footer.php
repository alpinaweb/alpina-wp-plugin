	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<?php dynamic_sidebar('sidebar-1'); ?>
				</div>
				<div class="col-lg-3">
					<?php dynamic_sidebar('sidebar-2'); ?>
				</div>
				<div class="col-lg-3">
					<?php dynamic_sidebar('sidebar-3'); ?>
				</div>
				<div class="col-lg-3 text-right">
					<p>Desenvolvido por: <a href="http://alpinaweb.com.br" target="_blank">Alpina</a></p>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="rubrica-alp"><a href="http://alpinaweb.com.br" target="_blank"></a></div>
		</div>
	</footer>

	<style media="screen">
		footer#footer {
			background: url(<?php echo $options['footer_bg']; ?>) center top;
    	background-size: auto;
			min-height: 340px;
		}
	</style>

	<?php wp_footer(); ?>


	<!-- <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script> -->
	<!-- <script src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/js/bootstrap.min.js"></script> -->
</body>
</html>
