<aside class="col d-none d-lg-block col-lg-3 col-sm-4 ml-5">

  <div class="widget mb-5 widget_search">
    <form role="search" method="get" class="search-form" action="/">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Buscar..." required="" value="" name="s">
        <input type="hidden" class="form-control" required="" value="post" name="post_type">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="submit" id="button-addon2">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
  </div>

  <?php if ( is_active_sidebar( 'alpina_wp_plugin_blog_sidebar' ) ) : ?>

    <?php dynamic_sidebar( 'alpina_wp_plugin_blog_sidebar' ); ?>

  <?php endif; ?>

</aside>
