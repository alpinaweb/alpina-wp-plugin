<article class="card mb-4 py-3 px-2">
  <div class="card-body">

    <?php if( has_post_thumbnail() ) { ?>
      <a href="<?php the_permalink(); ?>">
        <figure class="figure mb-5 featured">
          <?php the_post_thumbnail('large', array('class' => 'figure-img img-fluid rounded')); ?>
        </figure>
      </a>
    <?php } ?>

    <h6 class="card-subtitle mb-2"><strong><?php the_category( ', ' ); ?></strong></h6>
    <h2 class="card-title">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </h2>
    <?php if( has_excerpt() ) { ?>
      <p class="lead excerpt"><?php the_excerpt(); ?></p>
    <?php } ?>
    <p class="card-text mt-3">
      <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' atrás'; ?> - por: <span class="text-capitalize"><?php the_author_posts_link() ?></span>
    </p>
  </div>
</article>
