<?php
/**
* Adds Siga-nos widget
*/
class Single_Button_Widget extends WP_Widget {

	/**
	* Register widget with WordPress
	*/
	function __construct() {
		parent::__construct(
			'single_button', // Base ID
			esc_html__( 'ALP | Botão Simples', 'alpina-wp-plugin' ), // Name
			array( 'description' => esc_html__( 'Botão simples para Call To Action', 'alpina-wp-plugin' ), ) // Args
		);
	}

	/**
	* Widget Fields
	*/
	private $widget_fields = array(
		array(
			'label' => 'Texto',
			'id' => 'single_button_label',
			'type' => 'text',
		),
		array(
			'label' => 'Link',
			'id' => 'single_button_link',
			'type' => 'text',
		),
		array(
			'label' => 'Ícone',
			'id' => 'single_button_icon',
			'type' => 'text',
		),
		array(
			'label' => 'Cor',
			'id' => 'single_button_color',
			'type' => 'text',
		),
	);

	/**
	* Front-end display of widget
	*/
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		// Output widget title
    // echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ?: 'Siga-nos!' ) . $args['after_title'];

    echo '<ul class="">';

		// Output generated fields
		if ( $instance['single_button_label'] ) { ?>
      <li class="py-2">
        <a href="<?php echo $instance['single_button_link'] ?>" class="btn-cta btn btn-lg btn-block btn-primary">
					<i class="fas fa-lg fa-<?php echo $instance['single_button_icon'] ?>"></i> <?php echo $instance['single_button_label'] ?>
				</a>
      </li>

			<style media="screen">
				.btn-cta {
					background-color: <?php echo $instance['single_button_color'] ? $instance['single_button_color'] .' !important' : 'var(--alp-primary)' ?>; 
					border-color: <?php echo $instance['single_button_color'] ? $instance['single_button_color'] .' !important' : 'var(--alp-primary)' ?>					
				}
			</style>
    <?php };
    echo '</ul>';

		echo $args['after_widget'];
	}

	/**
	* Back-end widget fields
	*/
	public function field_generator( $instance ) {
		$output = '';
		foreach ( $this->widget_fields as $widget_field ) {
			$widget_value = ! empty( $instance[$widget_field['id']] ) ? $instance[$widget_field['id']] : esc_html__( @$widget_field['default'], 'alpina-wp-plugin' );
			switch ( $widget_field['type'] ) {
				default:
					$output .= '<p>';
					$output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'alpina-wp-plugin' ).':</label> ';
					$output .= '<input class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" type="'.$widget_field['type'].'" value="'.esc_attr( $widget_value ).'">';
					$output .= '</p>';
			}
		}
		echo $output;
	}

	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'alpina-wp-plugin' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'alpina-wp-plugin' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
		$this->field_generator( $instance );
	}

	/**
	* Sanitize widget form values as they are saved
	*/
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		foreach ( $this->widget_fields as $widget_field ) {
			switch ( $widget_field['type'] ) {
				case 'checkbox':
					$instance[$widget_field['id']] = $_POST[$this->get_field_id( $widget_field['id'] )];
					break;
				default:
					$instance[$widget_field['id']] = ( ! empty( $new_instance[$widget_field['id']] ) ) ? strip_tags( $new_instance[$widget_field['id']] ) : '';
			}
		}
		return $instance;
	}
} // class Single_Button_Widget

// register Siga-nos widget
function register_single_button_widget() {
	register_widget( 'Single_Button_Widget' );
}
add_action( 'widgets_init', 'register_single_button_widget' );
