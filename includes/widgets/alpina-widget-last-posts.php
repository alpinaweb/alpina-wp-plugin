<?php
/**
* Adds Last Blog Posts widget
*/
class Alpina_Widget_Last_Posts extends WP_Widget {

	/**
	* Register widget with WordPress
	*/
	function __construct() {
		parent::__construct(
			'lastblogposts_widget', // Base ID
			esc_html__( 'ALP | Carousel Últimos Posts', 'alpina_wp_plugin' ), // Name
			array( 'description' => esc_html__( 'Exiba os últimos posts do blog ao estilo "Alpina"', 'alpina_wp_plugin' ), ) // Args
		);
	}

	/**
	* Widget Fields
	*/
	private $widget_fields = array(
		array(
			'label'   => 'Qtd. Posts',
			'id'      => 'alp_qtp_posts',
			'default' => 3,
			'type'    => 'number',
		),
	);

	/**
	* Front-end display of widget
	*/
	public function widget( $args, $instance ) {
		echo @$args['before_widget'];

		// Output widget title
		// if ( ! empty( $instance['title'] ) ) {
		// 	echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		// }

		?>

		<?php
		$args = array( 'numberposts' => $instance['alp_qtp_posts'] ?: 3 );
		$recent_posts = wp_get_recent_posts( $args );
		$rand = rand() * 10;
		?>

		<section class="widget widget_recent_entries mb-5">
			<div id="carousel-<?php echo $rand ?>" class="carousel slide carousel-fade" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php
					$y = 0;
					foreach( $recent_posts as $key => $recent ) {
					?>
					<li data-target="#carousel-<?php echo $rand ?>" data-slide-to="<?php echo $key; ?>" class="<?php echo $y == 0 ? 'active' : ''; ?>"></li>
					<?php
					$y++;
					}
					?>
				</ol>
				<div class="carousel-inner" role="listbox">

					<?php
					$i = 0;
					foreach( $recent_posts as $recent ) {
					?>
					<div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
						<figure class="figure">
							<?php echo get_the_post_thumbnail($recent['ID'], 'large', ['class' => 'mx-auto d-block']) ?>
						</figure>
						<div class="carousel-caption d-none d-md-block">
							<p class="py-5">
								<strong><?php the_category( ', ' ); ?></strong>
							</p>
							<h6>
								<a href="<?php echo get_permalink($recent["ID"]) ?>"><?php echo $recent["post_title"] ?></a>
							</h6>
							<p class="py-5">
								<i class="far fa-clock"></i> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' atrás'; ?> <br>
								por: <?php the_author_posts_link() ?>
							</p>
						</div>
					</div>
					<?php
					$i++;
					}
					wp_reset_query();
					?>

				</div>
				<a class="left carousel-control" href="#carousel-<?php echo $rand ?>" role="button" data-slide="prev">
					<span class="icon-prev" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-<?php echo $rand ?>" role="button" data-slide="next">
					<span class="icon-next" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</section>

		<?php

		// Output generated fields
		// echo '<p>'.$instance['ttulo_86615'].'</p>';

		echo @$args['after_widget'];
	}

	/**
	* Back-end widget fields
	*/
	public function field_generator( $instance ) {
		$output = '';
		foreach ( $this->widget_fields as $widget_field ) {
			$widget_value = ! empty( $instance[$widget_field['id']] ) ? $instance[$widget_field['id']] : esc_html__( $widget_field['default'], 'alpina_wp_plugin' );
			switch ( $widget_field['type'] ) {
				default:
					$output .= '<p>';
					$output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'alpina_wp_plugin' ).':</label> ';
					$output .= '<input class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" type="'.$widget_field['type'].'" value="'.esc_attr( $widget_value ).'">';
					$output .= '</p>';
			}
		}
		echo $output;
	}

	public function form( $instance ) {
		$this->field_generator( $instance );
	}

	/**
	* Sanitize widget form values as they are saved
	*/
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		foreach ( $this->widget_fields as $widget_field ) {
			switch ( $widget_field['type'] ) {
				case 'checkbox':
					$instance[$widget_field['id']] = $_POST[$this->get_field_id( $widget_field['id'] )];
					break;
				default:
					$instance[$widget_field['id']] = ( ! empty( $new_instance[$widget_field['id']] ) ) ? strip_tags( $new_instance[$widget_field['id']] ) : '';
			}
		}
		return $instance;
	}
}
