<?php
/**
* Adds Last Blog Posts widget
*/
class Alpina_Widget_CTA extends WP_Widget {

	/**
	* Register widget with WordPress
	*/
	function __construct() {
		parent::__construct(
			'cta_widget', // Base ID
			esc_html__( 'ALP | CTA', 'alpina_wp_plugin' ), // Name
			array( 'description' => esc_html__( 'Chamada para ação', 'alpina_wp_plugin' ), ) // Args
		);

		// Add Widget scripts
		add_action('admin_enqueue_scripts', array($this, 'scripts'));
	}

	public function scripts () {

		wp_enqueue_script( 'media-upload' );
		wp_enqueue_media();

	}

	/**
	* Widget Fields
	*/
	private $widget_fields = array(
		array(
			'label'   => 'Título (1a palavra)',
			'id'      => 'title',
			'default' => 'eBook: ',
			'type'    => 'text',
		),
		array(
			'label'   => 'Texto',
			'id'      => 'text',
			'default' => '',
			'type'    => 'text',
		),
		array(
			'label'   => 'Imagem',
			'id'      => 'image',
			'default' => '',
			'type'    => 'text',
		),
		array(
			'label'   => 'Link botão',
			'id'      => 'link',
			'default' => '',
			'type'    => 'text',
		),
		array(
			'label'   => 'Texto botão',
			'id'      => 'label',
			'default' => '',
			'type'    => 'text',
		)
	);

	/**
	* Front-end display of widget
	*/
	public function widget( $args, $instance ) {
		echo @$args['before_widget'];

		?>

		<div class="card mb-4 p-4 p-md-3 cta">
		  <div class="card-body text-white align-middle">
	      <img class="mr-4 mb-3 img-fluid" src="<?php echo $instance['image'] ?>" alt="">
       	<h5 class="mt-md-0 mt-3 title"><span><?php echo $instance['title'] ?></span> <?php echo $instance['text'] ?></h5>
	      <a href="<?php echo $instance['link'] ?>" class="btn btn-outline-success btn-lg p-3 ml-3 mt-2"><?php echo $instance['label'] ?></a>
		  </div>
		</div>

		<style media="screen">
			@media (max-width: 1199px) {
				#alpina_wp_plugin_container .cta .title {
				    font-size: 22px;
				}
			}
		</style>

		<?php

		// Output generated fields
		// echo '<p>'.$instance['ttulo_86615'].'</p>';

		echo @$args['after_widget'];

	}

	/**
	* Back-end widget fields
	*/
	public function field_generator( $instance ) {
		$output = '';

		foreach ( $this->widget_fields as $widget_field ) {
			$widget_value = ! empty( $instance[$widget_field['id']] ) ? $instance[$widget_field['id']] : esc_html__( $widget_field['default'], 'alpina_wp_plugin' );
			switch ( $widget_field['type'] ) {
				default:
					$output .= '<p>';
					$output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'alpina_wp_plugin' ).':</label> ';
					$output .= '<input class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" type="'.$widget_field['type'].'" value="'.esc_attr( $widget_value ).'">';

					if ( $widget_field['id'] == 'image') {
						$output .= '<button class="upload_image_button button button-primary">Selecionar Imagem</button>';
					}

					$output .= '</p>';
			}
		}
		echo $output;
	}

	public function form( $instance ) {
		$this->field_generator( $instance );
	}

	/**
	* Sanitize widget form values as they are saved
	*/
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		foreach ( $this->widget_fields as $widget_field ) {
			switch ( $widget_field['type'] ) {
				case 'checkbox':
					$instance[$widget_field['id']] = $_POST[$this->get_field_id( $widget_field['id'] )];
					break;
				default:
					$instance[$widget_field['id']] = ( ! empty( $new_instance[$widget_field['id']] ) ) ? strip_tags( $new_instance[$widget_field['id']] ) : '';
			}
		}
		return $instance;
	}
}
