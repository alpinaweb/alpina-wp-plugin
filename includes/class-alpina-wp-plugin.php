<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://fb.com/luciano.tonet
 * @since      1.0.0
 *
 * @package    Alpina_Wp_Plugin
 * @subpackage Alpina_Wp_Plugin/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Alpina_Wp_Plugin
 * @subpackage Alpina_Wp_Plugin/includes
 * @author     Luciano <luciano@grupoalpina.com.br>
 */
class Alpina_Wp_Plugin {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Alpina_Wp_Plugin_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'alpina-wp-plugin';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

		$this->define_update_hooks();
		// $this->define_sidebars();
		$this->define_widgets_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Alpina_Wp_Plugin_Loader. Orchestrates the hooks of the plugin.
	 * - Alpina_Wp_Plugin_i18n. Defines internationalization functionality.
	 * - Alpina_Wp_Plugin_Admin. Defines all hooks for the admin area.
	 * - Alpina_Wp_Plugin_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-alpina-wp-plugin-loader.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-alpina-wp-plugin-updater.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/widgets/alpina-widget-last-posts.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/widgets/alpina-widget-single-button.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/widgets/alpina-widget-social.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/widgets/alpina-widget-cta.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-alpina-wp-plugin-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/alpina-wp-plugin-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-alpina-wp-plugin-public.php';


		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/alpina-wp-plugin-admin-menu.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/alpina-wp-plugin-admin-api.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/alpina-wp-plugin-admin-settings.php';


		$this->loader = new Alpina_Wp_Plugin_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Alpina_Wp_Plugin_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Alpina_Wp_Plugin_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Alpina_Wp_Plugin_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		// $this->loader->add_action( 'widgets_init', $plugin_admin, 'init_sidebars' );

		$this->loader->add_action( 'init', $plugin_admin, 'alpina_wp_plugin_settings_init');

		$this->loader->add_action( 'widgets_init', $plugin_admin, 'init_sidebars' );

		// $this->loader->add_action( 'admin_menu', $plugin_admin, 'alpina_wp_plugin_add_admin_menu' );
		// $this->loader->add_action( 'admin_init', $plugin_admin, 'alpina_wp_plugin_settings_init' );
		$this->loader->add_action( 'after_setup_theme', $this, 'alpina_add_theme_suport', 100);

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'wptuts_add_color_picker' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Alpina_Wp_Plugin_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_action('wp_head', $plugin_public, 'inject_custom_php_styles');

		// $this->loader->add_action( 'wp_head', $plugin_public, 'add_blog_style' );

		$this->loader->add_filter( 'template_include', $plugin_public, 'get_alpina_blog_template' );

		// $this->loader->add_filter( 'single_template', 	$plugin_public, 'get_alpina_single_template' );
		// $this->loader->add_filter( 'archive_template', 	$plugin_public, 'get_alpina_archive_template' ) ;
		// $this->loader->add_filter( 'page_template', 		$plugin_public, 'get_alpina_page_template' ) ;
		// $this->loader->add_filter( 'category_template', $plugin_public, 'get_alpina_category_template' ) ;
		// $this->loader->add_filter( 'author_template', 		$plugin_public, 'get_alpina_author_template' ) ;

		$this->loader->add_filter( 'get_search_form',   $plugin_public, 'alpina_search_form' );

	}

	private function define_update_hooks() {

		$plugin_updater = new Alpina_Wp_Plugin_Updater( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_filter('pre_set_site_transient_update_plugins', $plugin_updater, 'check_for_plugin_update');
		$this->loader->add_filter('plugins_api', $plugin_updater, 'plugin_api_call', 10, 3);

		// if (WP_DEBUG)
		// 	$this->loader->add_filter('plugins_api_result', $plugin_updater, 'api_debug_result', 10, 3);

	}

	public function define_widgets_hooks()
	{
		$this->loader->add_action( 'widgets_init', $this, 'register_widgets');
		// $this->loader->add_action( 'widgets_init', $this, 'register_sidebars');
	}

	public function register_widgets () {
		register_widget( 'Alpina_Widget_Last_Posts' );
		register_widget( 'Alpina_Widget_CTA' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Alpina_Wp_Plugin_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	public function alpina_add_theme_suport () {
			add_theme_support( 'post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat']);
	}

}
