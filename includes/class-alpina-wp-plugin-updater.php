<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://fb.com/luciano.tonet
 * @since      1.0.0
 *
 * @package    Alpina_Wp_Plugin
 * @subpackage Alpina_Wp_Plugin/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Alpina_Wp_Plugin
 * @subpackage Alpina_Wp_Plugin/includes
 * @author     Luciano <luciano@grupoalpina.com.br>
 */
class Alpina_Wp_Plugin_Updater {

	protected $api_url;
	protected $plugin_slug;

	public function __construct($plugin_name) {

		$this->api_url 			= 'http://central.alpdev.com.br/updates/';
		$this->plugin_slug 	= $plugin_name;

		// Take over the update check
		// add_filter('pre_set_site_transient_update_plugins', 'check_for_plugin_update');
	}

	public function check_for_plugin_update($checked_data) {
		global $wp_version;

		//Comment out these two lines during testing.
		// if (empty($checked_data->checked))
		// 	return $checked_data;

		$args = array(
			'slug' => $this->plugin_slug,
			// 'version' => ($checked_data->checked) ? $checked_data->checked[$this->plugin_slug .'/'. $this->plugin_slug .'.php'] : $checked_data->checked[$this->plugin_slug .'/'. $this->plugin_slug .'.php'],
		);
		$request_string = array(
				'body' => array(
					'action' => 'basic_check',
					'request' => serialize($args),
					'api-key' => md5(get_bloginfo('url'))
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
			);

		// Start checking for an update
		$raw_response = wp_remote_post($this->api_url, $request_string);

		if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
			$response = unserialize($raw_response['body']);

		if (is_object($response) && !empty($response)) // Feed the update data into WP updater
			$checked_data->response[$this->plugin_slug .'/'. $this->plugin_slug .'.php'] = $response;

		return $checked_data;
	}


	public function plugin_api_call($def, $action, $args) {
		global $wp_version;

		if (!isset($args->slug) || ($args->slug != $this->plugin_slug))
			return false;

		// Get the current version
		$plugin_info = get_site_transient('update_plugins');
		$current_version = $plugin_info->checked[$this->plugin_slug .'/'. $this->plugin_slug .'.php'];
		$args->version = $current_version;

		$request_string = array(
				'body' => array(
					'action' => $action,
					'request' => serialize($args),
					'api-key' => md5(get_bloginfo('url'))
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
			);

		$request = wp_remote_post($this->api_url, $request_string);

		if (is_wp_error($request)) {
			$res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
		} else {
			$res = unserialize($request['body']);

			if ($res === false)
				$res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
		}

		return $res;
	}

	public function api_debug_result($res, $action, $args) {
		echo "<pre>";
		print_r($res);
		echo "</pre>";
		// return $res;
		return;
	}

}
