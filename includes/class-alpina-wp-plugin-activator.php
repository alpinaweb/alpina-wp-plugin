<?php

/**
 * Fired during plugin activation
 *
 * @link       https://fb.com/luciano.tonet
 * @since      1.0.0
 *
 * @package    Alpina_Wp_Plugin
 * @subpackage Alpina_Wp_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Alpina_Wp_Plugin
 * @subpackage Alpina_Wp_Plugin/includes
 * @author     Luciano <luciano@grupoalpina.com.br>
 */
class Alpina_Wp_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
