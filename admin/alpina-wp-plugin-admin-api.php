<?php
class Alpina_WP_Plugin_Admin_API {
	/**
	 * Add routes
	 */
	public function add_routes( ) {
		register_rest_route( 'alpina-wp-plugin-api/v1', '/settings',
			array(
				'methods'         => 'POST',
				'callback'        => array( $this, 'update_settings' ),
				'args' => array(
					'header_bg' => array(
						'type' => 'string',
						'required' => false,
						'sanitize_callback' => 'sanitize_text_field'
					),
					'client_logo' => array(
						'type' => 'string',
						'required' => false,
						'sanitize_callback' => 'sanitize_text_field'
					),
					// 'option_01' => array(
					// 	'type' => 'string',
					// 	'required' => false,
					// 	'sanitize_callback' => 'sanitize_text_field'
					// ),
					// 'option_02' => array(
					// 	'type' => 'integer',
					// 	'required' => false,
					// 	'sanitize_callback' => 'absint'
					// ),
					"option_template_is_single" => array(
						"type" => "boolean",
						"required" => false
					),
					"option_template_is_search" => array(
						"type" => "boolean",
						"required" => false
					),
					"option_template_is_home" => array(
						"type" => "boolean",
						"required" => false
					),
					"option_template_is_author" => array(
						"type" => "boolean",
						"required" => false
					),
					"option_template_is_tag" => array(
						"type" => "boolean",
						"required" => false
					),
					"option_template_is_category" => array(
						"type" => "boolean",
						"required" => false
					),
					"option_template_is_archive" => array(
						"type" => "boolean",
						"required" => false
					),
					'main_color' => array(
						'type' => 'string',
						'required' => false
					),
					'secondary_color' => array(
						'type' => 'string',
						'required' => false
					),
					'footer_bg' => array(
						'type' => 'string',
						'required' => false,
						'sanitize_callback' => 'sanitize_text_field'
					),
					'inject_bootstrap' => array(
						'type' => 'boolean',
						'required' => false
					),
					'disqus_js' => array(
						'type' => 'string',
						'required' => false,
						'sanitize_callback' => 'sanitize_text_field'
					),
				),
				'permissions_callback' => array( $this, 'permissions' )
			)
		);
		register_rest_route( 'alpina-wp-plugin-api/v1', '/settings',
			array(
				'methods'         => 'GET',
				'callback'        => array( $this, 'get_settings' ),
				'args'            => array(),
				'permissions_callback' => array( $this, 'permissions' )
			)
		);
	}
	/**
	 * Check request permissions
	 *
	 * @return bool
	 */
	public function permissions(){
		return current_user_can( 'manage_options' );
	}
	/**
	 * Update settings
	 *
	 * @param WP_REST_Request $request
	 */
	public function update_settings( WP_REST_Request $request ){
		$settings = array(
			'header_bg' => $request->get_param( 'header_bg' ),
			'client_logo' => $request->get_param( 'client_logo' ),
			// 'option_01' => $request->get_param( 'option_01' ),
			// 'option_02' => $request->get_param( 'option_02' ),
			"option_template_is_single" => $request->get_param("option_template_is_single"),
			"option_template_is_search" => $request->get_param("option_template_is_search"),
			"option_template_is_home" => $request->get_param("option_template_is_home"),
			"option_template_is_author" => $request->get_param("option_template_is_author"),
			"option_template_is_tag" => $request->get_param("option_template_is_tag"),
			"option_template_is_category" => $request->get_param("option_template_is_category"),
			"option_template_is_archive" => $request->get_param("option_template_is_archive"),
			'main_color' => $request->get_param( 'main_color' ),
			'secondary_color' => $request->get_param( 'secondary_color' ),
			'footer_bg' => $request->get_param( 'footer_bg' ),
			'inject_bootstrap' => $request->get_param( 'inject_bootstrap' ),
			'disqus_js' => $request->get_param( 'disqus_js' )
		);
		Alpina_WP_Plugin_Admin_Settings::save_settings( $settings );
		return rest_ensure_response( Alpina_WP_Plugin_Admin_Settings::get_settings())->set_status( 201 );
	}
	/**
	 * Get settings via API
	 *
	 * @param WP_REST_Request $request
	 */
	public function get_settings( WP_REST_Request $request ){
		return rest_ensure_response( Alpina_WP_Plugin_Admin_Settings::get_settings());
	}
}
