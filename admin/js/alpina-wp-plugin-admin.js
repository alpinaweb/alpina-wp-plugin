jQuery( document ).ready(function() {

  'use strict'

  var $ = jQuery;

  if (typeof ALPINA_WP_PLUGIN !== 'undefined') {

    $('.color-field').wpColorPicker();

     $.ajax({
       method: 'GET',
       url: ALPINA_WP_PLUGIN.api.url,
       beforeSend: function (xhr) {
         xhr.setRequestHeader('X-WP-Nonce', ALPINA_WP_PLUGIN.api.nonce)
       }
     }).then(function (r) {

       if (r.hasOwnProperty('header_bg')) {
         console.log(r);
         $('#header_bg').val(r.header_bg)
         if(r.header_bg) {
           $('#header_bg_img').attr('src', r.header_bg)
         }
       }

       if (r.hasOwnProperty('client_logo')) {
         console.log(r);
         $('#client_logo').val(r.client_logo)
         if(r.client_logo) {
           $('#client_logo_img').attr('src', r.client_logo)
         }
       }

       if (r.hasOwnProperty('footer_bg')) {
         console.log(r);
         $('#footer_bg').val(r.footer_bg)
         if(r.footer_bg) {
           $('#footer_bg_img').attr('src', r.footer_bg)
         }
       }

       // if (r.hasOwnProperty('option_01')) {
       //   $('#option_01').val(r.option_01)
       // }
       //
       // if (r.hasOwnProperty('option_02')) {
       //   $('#option_02').val(r.option_02)
       // }

       if (r.hasOwnProperty("option_template_is_single")) {
         var checked = r.option_template_is_single
         $("#option_template_is_single").attr('checked', checked);
       }
       if (r.hasOwnProperty("option_template_is_search")) {
         var checked = r.option_template_is_search
         $("#option_template_is_search").attr('checked', checked);
       }
       if (r.hasOwnProperty("option_template_is_home")) {
         var checked = r.option_template_is_home
         $("#option_template_is_home").attr('checked', checked);
       }
       if (r.hasOwnProperty("option_template_is_author")) {
         var checked = r.option_template_is_author
         $("#option_template_is_author").attr('checked', checked);
       }
       if (r.hasOwnProperty("option_template_is_tag")) {
         var checked = r.option_template_is_tag
         $("#option_template_is_tag").attr('checked', checked);
       }
       if (r.hasOwnProperty("option_template_is_category")) {
         var checked = r.option_template_is_category
         $("#option_template_is_category").attr('checked', checked);
       }
       if (r.hasOwnProperty("option_template_is_archive")) {
         var checked = r.option_template_is_archive
         $("#option_template_is_archive").attr('checked', checked);
       }
       if (r.hasOwnProperty("inject_bootstrap")) {
         var checked = r.inject_bootstrap
         $("#inject_bootstrap").attr('checked', checked);
       }

       if (r.hasOwnProperty('main_color')) {
         $('#main_color').val(r.main_color)
       }
       if (r.hasOwnProperty('disqus_js')) {
         $('#disqus_js').val(r.disqus_js)
       }
       if (r.hasOwnProperty('secondary_color')) {
         $('#secondary_color').val(r.secondary_color)
       }
     })

     $('#alpina-wp-plugin-form').on('submit', function (e) {
       e.preventDefault()

       // var myForm = document.getElementById('myForm');
       // var formData = new FormData($('#alpina-wp-plugin-form'));
       // // Display the key/value pairs
       // for (var pair of formData.entries())
       // {
       //   console.log(pair[0]+ ', '+ pair[1]);
       // }

       var data = {
         header_bg: $('#header_bg').val(),
         client_logo: $('#client_logo').val(),
         option_02: $('#option_02').val(),
         option_01: $('#option_01').val(),
         option_template_is_single: document.getElementById('option_template_is_single').checked,
         option_template_is_search: document.getElementById('option_template_is_search').checked,
         option_template_is_home: document.getElementById('option_template_is_home').checked,
         option_template_is_author: document.getElementById('option_template_is_author').checked,
         option_template_is_tag: document.getElementById('option_template_is_tag').checked,
         option_template_is_category: document.getElementById('option_template_is_category').checked,
         option_template_is_archive: document.getElementById('option_template_is_archive').checked,
         main_color: $('#main_color').val(),
         secondary_color: $('#secondary_color').val(),
         footer_bg: $('#footer_bg').val(),
         inject_bootstrap: document.getElementById('inject_bootstrap').checked,
         disqus_js: document.getElementById('disqus_js').value,
       }

       $.ajax({
         method: 'POST',
         url: ALPINA_WP_PLUGIN.api.url,
         beforeSend: function (xhr) {
           xhr.setRequestHeader('X-WP-Nonce', ALPINA_WP_PLUGIN.api.nonce)
         },
         data: data
       }).then(function (r) {
         swal("Salvo!", ALPINA_WP_PLUGIN.strings.saved, "success");
       })
       .fail(function (r) {
         var message = ALPINA_WP_PLUGIN.strings.error
         if (r.hasOwnProperty('message')) {
           message = r.message
         }
         swal("Oh no!", message, "error");
       })
     })


     // The "Upload" button
    $('.upload_image_button').click(function() {
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);
        wp.media.editor.send.attachment = function(props, attachment) {
            $(button).parent().prev().attr('src', attachment.url);
            $(button).prev().val(attachment.url);
            wp.media.editor.send.attachment = send_attachment_bkp;
        }
        wp.media.editor.open(button);
        return false;
    });

    // The "Remove" button (remove the value from input type='hidden')
    $('.remove_image_button').click(function() {
        var answer = confirm('Are you sure?');
        if (answer == true) {
            var src = $(this).parent().prev().attr('data-src');
            $(this).parent().prev().attr('src', src);
            $(this).prev().prev().val('');
        }
        return false;
    });

  };

});


jQuery(document).ready(function ($) {

 $(document).on("click", ".upload_image_button", function (e) {
    e.preventDefault();
    var $button = $(this);


    // Create the media frame.
    var file_frame = wp.media.frames.file_frame = wp.media({
       title: 'Select or upload image',
       library: { // remove these to show all
          type: 'image' // specific mime
       },
       button: {
          text: 'Select'
       },
       multiple: false  // Set to true to allow multiple files to be selected
    });

    // When an image is selected, run a callback.
    file_frame.on('select', function () {
       // We set multiple to false so only get one image from the uploader

       var attachment = file_frame.state().get('selection').first().toJSON();

       $button.siblings('input').val(attachment.url);

    });

    // Finally, open the modal
    file_frame.open();
 });
});
