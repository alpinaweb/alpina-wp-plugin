<?php
class Alpina_WP_Plugin_Admin_Menu {
	/**
	 * Menu slug
	 *
	 * @var string
	 */
	protected $slug = 'alpina-wp-plugin-menu';
	/**
	 * URL for assets
	 *
	 * @var string
	 */
	protected $assets_url;
	/**
	 * Alpina_WP_Plugin_Menu constructor.
	 *
	 * @param string $assets_url URL for assets
	 */
	public function __construct( $assets_url ) {
		$this->assets_url = $assets_url;
		add_action( 'admin_menu', array( $this, 'add_page' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_assets' ) );
	}
	/**
	 * Add CF Popup submenu page
	 *
	 * @since 0.0.3
	 *
	 * @uses "admin_menu"
	 */
	public function add_page(){
		add_menu_page(
			__( 'Alpina WP Plugin', 'text-domain' ),
			__( 'Alpina WP Plugin', 'text-domain' ),
			'manage_options',
			$this->slug,
			array( $this, 'render_admin' ) );
	}
	/**
	 * Register CSS and JS for page
	 *
	 * @uses "admin_enqueue_scripts" action
	 */
	public function register_assets()
	{

		// wp_register_script( $this->slug, $this->assets_url . '/app/dist/alpina-wp-plugin-admin.js', array( 'jquery' ), microtime() );
		wp_register_script( $this->slug, $this->assets_url . '/js/admin.js', 										array( 'jquery' ), microtime() );

		wp_register_style( $this->slug, $this->assets_url . '/css/admin.css', microtime());
		wp_localize_script( $this->slug, 'ALPINA_WP_PLUGIN', array(
			'strings' => array(
				'saved' => __( 'Configurações salvas com sucesso!', 'text-domain' ),
				'error' => __( 'Não foi possível salvar as informações. Recarregue a página e tente salvar novamente.', 'text-domain' )
			),
			'api'     => array(
				'url'   => esc_url_raw( rest_url( 'alpina-wp-plugin-api/v1/settings' ) ),
				'nonce' => wp_create_nonce( 'wp_rest' )
			)
		) );
	}
	/**
	 * Enqueue CSS and JS for page
	 */
	public function enqueue_assets(){
		if( ! wp_script_is( $this->slug, 'registered' ) ){
			$this->register_assets();
		}
		wp_enqueue_script( $this->slug );
		wp_enqueue_style( $this->slug );
	}
  /**
  	 * Render plugin admin page
  	 */
  	public function render_admin(){
  		$this->enqueue_assets();
			$options = get_option( '_alpina_settings' );
  		?>

			<div>
			  <h1>Alpina WP Plugin Settings</h1>
			</div>


  		<form id="alpina-wp-plugin-form">
  			<div>
						<label for="client_logo">Logo do cliente</label>
						<?php Alpina_WP_Plugin_Admin_Settings::image_uploader( 'client_logo', $width = 115, $height = 115 ); ?>
  			</div>
				<div>
					<label for="header_bg">Imagem do sub-cabeçalho</label>
					<?php Alpina_WP_Plugin_Admin_Settings::image_uploader( 'header_bg', $width = 115, $height = 115 ); ?>
				</div>
				<div>
					<label for="">Utilizar templates</label>
					<label for="option_template_is_single">
						<input id="option_template_is_single" type="checkbox" name="option_template_is_single" <?php checked( 1, @$options["option_template_is_single"], false ) ?>/>
						Single
					</label>
				</div>
				<div>
					<label for=""></label>
					<label for="option_template_is_search">
						<input id="option_template_is_search" type="checkbox" name="option_template_is_search" <?php checked( 1, @$options["option_template_is_search"], false ) ?>/>
						Search
					</label>
				</div>
				<div>
					<label for=""></label>
					<label for="option_template_is_home">
						<input id="option_template_is_home" type="checkbox" name="option_template_is_home" <?php checked( 1, @$options["option_template_is_home"], false ) ?>/>
						Home
					</label>
				</div>
				<div>
					<label for=""></label>
					<label for="option_template_is_author">
						<input id="option_template_is_author" type="checkbox" name="option_template_is_author" <?php checked( 1, @$options["option_template_is_author"], false ) ?>/>
						Author
					</label>
				</div>
				<div>
					<label for=""></label>
					<label for="option_template_is_tag">
						<input id="option_template_is_tag" type="checkbox" name="option_template_is_tag" <?php checked( 1, @$options["option_template_is_tag"], false ) ?>/>
						Tag
					</label>
				</div>
				<div>
					<label for=""></label>
					<label for="option_template_is_category">
						<input id="option_template_is_category" type="checkbox" name="option_template_is_category" <?php checked( 1, @$options["option_template_is_category"], false ) ?>/>
						Category
					</label>
				</div>
				<div>
					<label for=""></label>
					<label for="option_template_is_archive">
						<input id="option_template_is_archive" type="checkbox" name="option_template_is_archive" <?php checked( 1, @$options["option_template_is_archive"], false ) ?>/>
						Archive
					</label>
				</div>
  			<!-- <div>
  				<label for="option_01">Option 01</label>
  				<input id="option_01" type="text" />
  			</div>
  			<div>
  				<label for="option_02">Option 02</label>
  				<input id="option_02" type="number" min="0" max="100" />
  			</div> -->
				<div>
					<label for="header_bg">Cor principal</label>
					<input id="main_color" type="text" value="<?php echo @$options['main_color'] ?>" class="color-field">
				</div>
				<div>
					<label for="header_bg">Cor secundária</label>
					<input id="secondary_color" type="text" value="<?php echo @$options['secondary_color'] ?>" class="color-field">
				</div>
				<div>
					<label for="header_bg">Imagem do rodapé</label>
					<?php Alpina_WP_Plugin_Admin_Settings::image_uploader( 'footer_bg', $width = 115, $height = 115 ); ?>
				</div>

				<hr>

				<div>
					<label for="">Incorporar Bootstrap 4.1.3</label>
					<label for="inject_bootstrap">
						<input id="inject_bootstrap" type="checkbox" name="inject_bootstrap" <?php checked( 1, @$options["inject_bootstrap"], false ) ?>/>
						Incorporar
					</label>
				</div>

				<hr>

				<div>
  				<label for="disqus_js">Disqus JS</label>
  				<input id="disqus_js" type="text" name="disqus_js"/>
  			</div>

  			<?php submit_button( __( 'Salvar', 'text-domain' ) ); ?>
  		</form>

  		<?php
  	}
}
