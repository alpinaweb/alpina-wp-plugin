<?php
class Alpina_WP_Plugin_Admin_Settings {
	/**
	 * Option key to save settings
	 *
	 * @var string
	 */
	protected static $option_key = '_alpina_settings';
	/**
	 * Default settings
	 *
	 * @var array
	 */
	protected static $defaults = array(
		'header_bg' => '',
		'client_logo' => '',
		// 'option_01' => '',
		// 'option_02' => '',
		"option_template_is_single" => true,
		"option_template_is_search" => true,
		"option_template_is_home" => true,
		"option_template_is_author" => true,
		"option_template_is_tag" => true,
		"option_template_is_category" => true,
		"option_template_is_archive" => true,
		'main_color' => '#491dc8',
		'secondary_color' => '#01a5ff',
		'footer_bg' => '',
		'inject_bootstrap' => '',
		'disqus_js' => 'https://alpdev.disqus.com/embed.js',
	);
	/**
	 * Get saved settings
	 *
	 * @return array
	 */
	public static function get_settings(){
		$saved = get_option( self::$option_key, array() );

		if( ! is_array( $saved ) || empty( $saved )){
			return self::$defaults;
		}

		return wp_parse_args( $saved, self::$defaults );
	}
	/**
	 * Save settings
	 *
	 * Array keys must be whitelisted (IE must be keys of self::$defaults
	 *
	 * @param array $settings
	 */
	public static function save_settings( array  $settings ){
		//remove any non-allowed indexes before save
		foreach ( $settings as $key => $setting ){
			if( ! array_key_exists( $key, self::$defaults ) ){
				unset( $settings[ $key ] );
			}
		}
		update_option( self::$option_key, $settings );
	}

	/**
	 * Image Uploader
	 *
	 * author: Arthur Gareginyan www.arthurgareginyan.com
	 */
	static function image_uploader( $name, $width, $height ) {

	    // Set variables
	    // $options = get_option( 'alp_settings_header_bg' );
	    $options = get_option( '_alpina_settings' );
	    $default_image = plugins_url('img/no-image.png', __FILE__);


	    if ( !empty( $options[$name] ) ) {
	        // $image_attributes = wp_get_attachment_image_src( $options[$name], array( $width, $height ) );
	        // $image_attributes = $options[$name];
	        $src = $options[$name];
	        $value = $options[$name];
	    } else {
	        $src = $default_image;
	        $value = '';
	    }

			// var_dump($src);
			// exit;
	    $text = __( 'Enviar imagem', 'RSSFI_TEXT' );

	    // Print HTML field
	    echo '
      <div class="upload">
          <img data-src="'.$default_image.'" src="' . $src . '" width="' . $width . 'px" height="auto" id="' . $name . '_img" />
          <div>
              <input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
              <button type="submit" class="upload_image_button button">' . $text . '</button>
              <button type="submit" class="remove_image_button button">&times;</button>
          </div>
      </div>
	    ';
	}

}
