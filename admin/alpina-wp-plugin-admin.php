<?php

/**
* The admin-specific functionality of the plugin.
*
* @link       https://fb.com/luciano.tonet
* @since      1.0.0
*
* @package    Alpina_Wp_Plugin
* @subpackage Alpina_Wp_Plugin/admin
*/

/**
* The admin-specific functionality of the plugin.
*
* Defines the plugin name, version, and two examples hooks for how to
* enqueue the admin-specific stylesheet and JavaScript.
*
* @package    Alpina_Wp_Plugin
* @subpackage Alpina_Wp_Plugin/admin
* @author     Luciano <luciano@grupoalpina.com.br>
*/
class Alpina_Wp_Plugin_Admin {

	/**
	* The ID of this plugin.
	*
	* @since    1.0.0
	* @access   private
	* @var      string    $plugin_name    The ID of this plugin.
	*/
	private $plugin_name;
		/**
	* The version of this plugin.
	*
	* @since    1.0.0
	* @access   private
	* @var      string    $version    The current version of this plugin.
	*/
	private $version;

	/**
	* Initialize the class and set its properties.
	*
	* @since    1.0.0
	* @param      string    $plugin_name       The name of this plugin.
	* @param      string    $version    The version of this plugin.
	*/
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	* Register the stylesheets for the admin area.
	*
	* @since    1.0.0
	*/
	public function enqueue_styles() {

		/**
		* This function is provided for demonstration purposes only.
		*
		* An instance of this class should be passed to the run() function
		* defined in Alpina_Wp_Plugin_Loader as all of the hooks are defined
		* in that particular class.
		*
		* The Alpina_Wp_Plugin_Loader will then create the relationship
		* between the defined hooks and the functions defined in this
		* class.
		*/

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . '/css/alpina-wp-plugin-admin.css', array(), $this->version, 'all' );

	}

	/**
	* Register the JavaScript for the admin area.
	*
	* @since    1.0.0
	*/
	public function enqueue_scripts() {

		/**
		* This function is provided for demonstration purposes only.
		*
		* An instance of this class should be passed to the run() function
		* defined in Alpina_Wp_Plugin_Loader as all of the hooks are defined
		* in that particular class.
		*
		* The Alpina_Wp_Plugin_Loader will then create the relationship
		* between the defined hooks and the functions defined in this
		* class.
		*/
		wp_enqueue_script( 'sweetalert', 	plugin_dir_url( __FILE__ ) . '/js/sweetalert.min.js', array( 'jquery' ), $this->version . microtime(), false );
		wp_enqueue_script( $this->plugin_name, 	plugin_dir_url( __FILE__ ) . '/js/alpina-wp-plugin-admin.js', array( 'jquery' ), $this->version . microtime(), false );

		wp_enqueue_media();

	}

	public function alpina_wp_plugin_settings_init () {
		$assets_url = plugin_dir_url( __FILE__ );
		//Setup menu
		// if( is_admin() ){
			new Alpina_WP_Plugin_Admin_Menu( $assets_url );
		// }
		//Setup REST API
		$api = new Alpina_WP_Plugin_Admin_API();
		$api->add_routes();
	}

	public function init_sidebars()
	{
			$args = array(
				'name'          => 'ALP | Blog Sidebar',
				'id'          	=> 'alpina_wp_plugin_blog_sidebar',
				'description'   => 'Sidebar do blog',
				'before_widget' => '<div id="%1$s" class="widget mb-5 %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widget-title text-uppercase">',
				'after_title'   => '</h2>',
			);
			register_sidebar($args);

			$args = array(
				'name'          => 'ALP | Blog Subheader',
				'id'          	=> 'alpina_wp_plugin_blog_subheader',
				'description'   => 'Subheader do blog',
				'before_widget' => '<div id="%1$s" class="widget mb-5 %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title text-uppercase h2">',
				'after_title'   => '</h4>',
			);
			register_sidebar($args);

			$args = array(
				'name'          => 'ALP | Blog CTA',
				'id'          	=> 'alpina_wp_plugin_blog_cta',
				'description'   => 'Call to action exibida no final da listagem de artigos',
				'before_widget' => '<div id="%1$s" class="%2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widget-title text-uppercase">',
				'after_title'   => '</h2>',
			);
			register_sidebar($args);
	}

	public function wptuts_add_color_picker( $hook ) {

	    if( is_admin() ) {

	        // Add the color picker css file
	        wp_enqueue_style( 'wp-color-picker' );

	        // Include our custom jQuery file with WordPress Color Picker dependency
	        wp_enqueue_script( 'custom-script-handle', plugins_url( 'custom-script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
	    }
	}

}
